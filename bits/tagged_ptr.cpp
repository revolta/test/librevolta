#include <gtest/gtest.h>

#include "memory.hpp"

namespace ReVolta {

	class alignas( 4096 ) sample_base : public use_counter<sample_base> {
	public:
		sample_base( void ) noexcept = default;

		sample_base( unsigned char value ) noexcept : m_value{ value } {}

		unsigned char get_value( void ) noexcept { return m_value; }

	private:
		unsigned char m_value;
	};

	/* Tags partial specialization for 'sample_base' class */
	template<typename TAGGED_POINTER>
	struct tags<TAGGED_POINTER, sample_base> {
		constexpr static size_t required_tags_bit_width { 1 };

		void set_lsb( bool value ) noexcept {
			if( value ) {
				( static_cast<TAGGED_POINTER*>( this ) )->raw() |= 0x01;
			} else {
				static_cast<TAGGED_POINTER*>( this )->raw() &= 0x00;
			}
		}

		bool get_lsb( void ) noexcept {
			return static_cast<TAGGED_POINTER*>( this )->raw() & 0x01;
		}
	};

}

using namespace ReVolta;

TEST( tagged_ptr, unique_ptr ) {
	auto sample = allocate_tagged<unique_ptr, sample_base>( allocator<sample_base>(), 42 );

	EXPECT_EQ( sample->get_value(), 42 );
#if false
	uintptr_t address = sample.raw();

	sample.set_lsb( true );
	EXPECT_EQ( ( address + 1 ), sample.raw() );
#endif
}

TEST( tagged_ptr, pointer_traits ) {
	auto sample = allocate_tagged<unique_ptr, sample_base>( allocator<sample_base>(), 42 );

	EXPECT_EQ( pointer_traits<decltype( sample )>::is_tagged::value, true );
}

TEST( tagged_ptr, unique_ptr_move ) {
	auto first = allocate_tagged<unique_ptr, sample_base>( allocator<sample_base>(), 11 );
	auto second = allocate_tagged<unique_ptr, sample_base>( allocator<sample_base>(), 22 );

	EXPECT_EQ( first->get_value(), 11 );
	EXPECT_EQ( second->get_value(), 22 );

#if false
	const uintptr_t first_raw = first.raw();
#endif

	first.set_lsb( true );

	EXPECT_EQ( first->get_value(), 11 );
#if false
	EXPECT_EQ( first.raw(), first_raw + 1 );
#endif
	second = move( first );

	EXPECT_EQ( first.get(), nullptr );
	EXPECT_EQ( second->get_value(), 11 );
#if false
	EXPECT_EQ( second.raw(), first_raw + 1 );
#endif
}

TEST( tagged_ptr, make_tagged ) {
	unique_ptr<sample_base> unique_sample = allocate_unique<sample_base>( allocator<sample_base>(), 42 );
	EXPECT_EQ( unique_sample->get_value(), 42 );
	EXPECT_TRUE( unique_sample );

	auto tagged_sample = make_tagged( move( unique_sample ) );

	EXPECT_EQ( tagged_sample->get_value(), 42 );
	EXPECT_TRUE( tagged_sample );
	EXPECT_EQ( unique_sample.get(), nullptr );
}

TEST( tagged_ptr, observer_ptr ) {
	unique_ptr<sample_base> unique_sample = allocate_unique<sample_base>( allocator<sample_base>(), 42 );
	auto tagged_sample = make_tagged( make_observer( unique_sample ) );

	tagged_sample.set_lsb( true );

	EXPECT_EQ( tagged_sample->get_value(), 42 );
	EXPECT_EQ( tagged_sample.get_lsb(), true );
}


TEST( tagged_ptr, retain_ptr ) {
	auto sample = allocate_tagged<retain_ptr, sample_base>( allocator<sample_base>(), 66 );

	EXPECT_EQ( sample->get_value(), 66 );
	EXPECT_EQ( sample.use_count(), 1 );
#if false
	uintptr_t raw = sample.raw();

	sample.set_lsb( true );
	EXPECT_EQ( raw + 1, sample.raw() );
#endif
}

TEST( tagged_ptr, retain_ptr_copy ) {
	auto first = allocate_tagged<retain_ptr, sample_base>( allocator<sample_base>(), 11 );
	auto second = allocate_tagged<retain_ptr, sample_base>( allocator<sample_base>(), 22 );

	EXPECT_EQ( first->get_value(), 11 );
	EXPECT_EQ( second->get_value(), 22 );

	EXPECT_EQ( first.use_count(), 1 );
	EXPECT_EQ( second.use_count(), 1 );

	first.set_lsb( true );

	second = first;

	EXPECT_EQ( first->get_value(), 11 );
	EXPECT_EQ( second->get_value(), 11 );

	EXPECT_EQ( first.use_count(), 2 );
	EXPECT_EQ( second.use_count(), 2 );
#if false
	EXPECT_EQ( first.raw(), second.raw() );
#endif
}



TEST( tagged_ptr, retain_ptr_move ) {
	auto first = allocate_tagged<retain_ptr, sample_base>( allocator<sample_base>(), 11 );
	auto second = allocate_tagged<retain_ptr, sample_base>( allocator<sample_base>(), 22 );

	EXPECT_EQ( first->get_value(), 11 );
	EXPECT_EQ( second->get_value(), 22 );

	EXPECT_EQ( first.use_count(), 1 );
	EXPECT_EQ( second.use_count(), 1 );

	first.set_lsb( true );
#if false
	uintptr_t first_raw = first.raw();
#endif
	second = move( first );

	EXPECT_EQ( first.get(), nullptr );
#if false
	EXPECT_EQ( second.raw(), first_raw );
#endif

	EXPECT_EQ( first.use_count(), 0 );
	EXPECT_EQ( second.use_count(), 1 );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}