#include <gtest/gtest.h>

#include "memory.hpp"

namespace ReVolta {

	struct sample_base {
		sample_base( void ) = default;

		sample_base( unsigned char value ) noexcept : m_value{ value } {}

		void base_function( void ) noexcept {}

		unsigned char m_value { 42 };
	};

	struct sample_derived : public sample_base {
		sample_derived( void ) noexcept = default;

		sample_derived( unsigned char value ) noexcept : sample_base( value ) {}

		void derived_function( void ) noexcept {}
	};

}

using namespace ReVolta;

TEST( unique_ptr, default_constructor ) {
	/* Construct unique_ptr by default constructor - owns nothing */
	unique_ptr<sample_base> default_constructed_ptr;
	EXPECT_EQ( default_constructed_ptr.get(), nullptr );
}

TEST( unique_ptr, nullptr_constructor ) {
	/* Construct unique_ptr by nullptr */
	unique_ptr<sample_base> nullptr_constructed_ptr( nullptr );
	EXPECT_EQ( nullptr_constructed_ptr.get(), nullptr );
}

TEST( unique_ptr, explicit_constructor ) {
	unique_ptr<sample_base> sample = allocate_unique<sample_base>( allocator<sample_base>() );
	EXPECT_TRUE( sample.get() != nullptr );
}

TEST( unique_ptr, converting_constructor ) {
	unique_ptr<sample_derived> source( new sample_derived() );
	unique_ptr<sample_base> sample( move( source ) );
	EXPECT_TRUE( sample.get() != nullptr );

	sample->base_function();
}

TEST( unique_ptr, converting_assignment ) {
	unique_ptr<sample_derived> source( new sample_derived() );
	EXPECT_TRUE( source.get() != nullptr );
	sample_derived * address = source.get();
	unique_ptr<sample_base> sample;
	EXPECT_TRUE( sample.get() == nullptr );
	sample = move( source );
	EXPECT_TRUE( source.get() == nullptr );
	EXPECT_TRUE( sample.get() != nullptr );
	EXPECT_EQ( static_cast<sample_base *>( address ), sample.get() );
}

TEST( unique_ptr, move_constructor ) {
	unique_ptr<sample_base> first( new sample_base() );
	const sample_base * first_ptr = first.get();
	EXPECT_TRUE( first.get() != nullptr );
	unique_ptr<sample_base> second( move( first ) );
	EXPECT_EQ( first.get(), nullptr );
	EXPECT_EQ( second.get(), first_ptr );
}

TEST( unique_ptr, pointer_dereference ) {
	unique_ptr<sample_base> sample( new sample_base( 66 ) );
	EXPECT_EQ( sample->m_value, 66 );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}