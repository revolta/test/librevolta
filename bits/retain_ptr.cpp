#include <gtest/gtest.h>

#include "memory.hpp"

namespace ReVolta {

    struct sample_base : public use_counter<sample_base> {
        sample_base( void ) noexcept : m_value( 0 ) {}

        sample_base( unsigned char value ) noexcept : m_value( value ) {}

        unsigned char get_value( void ) noexcept {
            return m_value;
        }

    private:
        unsigned char m_value;
    };

    struct sample_derived : public sample_base {
        sample_derived( void ) noexcept = default;

        sample_derived( unsigned char value ) noexcept : sample_base( value ) {}
    };

}

using namespace ReVolta;

TEST( retain_ptr, default_constructor ) {
    retain_ptr<sample_base> sample;
    EXPECT_EQ( sample.get(), nullptr );
}

TEST( retain_ptr, nullptr_constructor ) {
    retain_ptr<sample_base> sample( nullptr );
    EXPECT_EQ( sample.get(), nullptr );
}

TEST( retain_ptr, explicit_constructor ) {
    retain_ptr<sample_base> sample( new sample_base() );
    EXPECT_TRUE( sample.get() != nullptr );
    EXPECT_EQ( sample.use_count(), 1 );
}

TEST( retain_ptr, copy_constructor ) {
    retain_ptr<sample_base> first( new sample_base() );
    EXPECT_EQ( first.use_count(), 1 );

    retain_ptr<sample_base> second( first );
    EXPECT_EQ( second.use_count(), 2 );
}

TEST( retain_ptr, move_constructor ) {
    retain_ptr<sample_base> first( new sample_base() );
    EXPECT_EQ( first.use_count(), 1 );

    retain_ptr<sample_base> second( move( first ) );

    EXPECT_EQ( first.get(), nullptr );
    EXPECT_EQ( second.use_count(), 1 );
}

TEST( retain_ptr, converting_copy_constructor ) {
    retain_ptr<sample_derived> derived( new sample_derived() );
    EXPECT_EQ( derived.use_count(), 1 );

    retain_ptr<sample_base> base( derived );

    EXPECT_EQ( base.use_count(), 2 );
    EXPECT_EQ( derived.use_count(), 2 );
}

TEST( retain_ptr, converting_move_constructor ) {
    retain_ptr<sample_derived> derived( new sample_derived() );
    EXPECT_EQ( derived.use_count(), 1 );
    void * ptr = derived.get();

    retain_ptr<sample_base> base( move( derived ) );

    EXPECT_EQ( derived.get(), nullptr );
    EXPECT_EQ( base.get(), ptr );
    /* Expect use count to be 1 as the instance becomes to be be retained by the 'base' */
    EXPECT_EQ( base.use_count(), 1 );
    /* Expect zero use count as derived is no longer retaining the instance */
    EXPECT_EQ( derived.use_count(), 0 );
}

TEST( retain_ptr, pointer_dereference ) {
    retain_ptr<sample_base> sample( new sample_base( 42 ) );
    EXPECT_EQ( sample->get_value(), 42 );
}

TEST( retain_ptr, nullptr_assignment ) {
    retain_ptr<sample_base> sample( new sample_base( 42 ) );
    EXPECT_EQ( sample->get_value(), 42 );
    EXPECT_EQ( sample.use_count(), 1 );

    sample = nullptr;

    EXPECT_EQ( sample.get(), nullptr );
    EXPECT_EQ( sample.use_count(), 0 );
}

TEST( retain_ptr, copy_assignment ) {
    retain_ptr<sample_base> first( new sample_base( 42 ) );
    EXPECT_EQ( first->get_value(), 42 );
    EXPECT_EQ( first.use_count(), 1 );

    retain_ptr<sample_base> second( nullptr );
    EXPECT_EQ( second.get(), nullptr );

    second = first;

    EXPECT_EQ( first.use_count(), 2 );
    EXPECT_EQ( second.use_count(), 2 );
    EXPECT_EQ( first->get_value(), second->get_value() );

    first = nullptr;

    EXPECT_EQ( second.use_count(), 1 );
    EXPECT_EQ( second->get_value(), 42 );
}

TEST( retain_ptr, move_assignment ) {
    retain_ptr<sample_base> first( new sample_base( 42 ) );
    EXPECT_EQ( first->get_value(), 42 );
    EXPECT_EQ( first.use_count(), 1 );

    retain_ptr<sample_base> second( nullptr );
    EXPECT_EQ( second.get(), nullptr );

    second = move( first );

    EXPECT_EQ( first.use_count(), 0 );
    EXPECT_EQ( first.get(), nullptr );
    EXPECT_EQ( second.use_count(), 1 );
    EXPECT_EQ( second->get_value(), 42 );
}

/* FIXME: Somehow non-converting assignment operator is used */
TEST( retain_ptr, converting_assignment ) {
    retain_ptr<sample_derived> derived( new sample_derived( 42 ) );
    EXPECT_EQ( derived->get_value(), 42 );
    EXPECT_EQ( derived.use_count(), 1 );

    retain_ptr<sample_base> base( nullptr );
    EXPECT_EQ( base.get(), nullptr );

    base = move( derived );

    EXPECT_EQ( base->get_value(), 42 );
    EXPECT_EQ( derived.use_count(), 0 );
    EXPECT_EQ( derived.get(), nullptr );
}

TEST( retain_ptr, operator_bool ) {
    retain_ptr<sample_base> sample( new sample_base( 42 ) );
    EXPECT_EQ( static_cast<bool>( sample ), true );
    sample = nullptr;
    EXPECT_EQ( static_cast<bool>( sample ), false );
}

TEST( retain_ptr, detach ) {
    retain_ptr<sample_base> sample( new sample_base( 42 ) );
    EXPECT_EQ( sample.use_count(), 1 );

    retain_ptr<sample_base> other( sample );
    EXPECT_EQ( sample.use_count(), 2 );
    EXPECT_EQ( other.use_count(), 2 );

    sample.detach();

    EXPECT_EQ( other.use_count(), 1 );
    EXPECT_EQ( sample.get(), nullptr );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}