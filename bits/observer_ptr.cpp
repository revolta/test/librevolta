#include <gtest/gtest.h>

#include "memory.hpp"

namespace ReVolta {
    
    struct sample_base {
        sample_base( void ) noexcept : m_value( 0 ) {}

        sample_base( unsigned char value ) noexcept : m_value( value ) {}

        unsigned char get_value( void ) noexcept {
            return m_value;
        }

    private:
        unsigned char m_value;
    };

    struct sample_derived : public sample_base {
        sample_derived( void ) noexcept = default;

        sample_derived( unsigned char value ) noexcept : sample_base( value ) {}
    };

}

using namespace ReVolta;

TEST( observer_ptr, default_constructor ) {
    observer_ptr<sample_base> sample;
    EXPECT_EQ( sample.get(), nullptr );
}

TEST( observer_ptr, nullptr_constructor ) {
    observer_ptr<sample_base> sample( nullptr );
    EXPECT_EQ( sample.get(), nullptr );
}

TEST( observer_ptr, explicit_constructor ) {
    observer_ptr<sample_base> sample( new sample_base( 42 ) );
    EXPECT_TRUE( sample.get() != nullptr );
    EXPECT_EQ( sample->get_value(), 42 );
}

TEST( observer_ptr, make_observer_unique_ptr ) {
    unique_ptr<sample_base> first = allocate_unique<sample_base>( allocator<sample_base>(), 42 );
    EXPECT_EQ( first->get_value(), 42 );

    observer_ptr<sample_base> sample = make_observer( first );
    EXPECT_EQ( sample->get_value(), first->get_value() );

    unique_ptr<sample_base> second = move( first );
    EXPECT_EQ( first.get(), nullptr );
    EXPECT_EQ( second->get_value(), sample->get_value() );
}

TEST( observer_ptr, copy_constructor ) {
    unique_ptr<sample_base> observable = allocate_unique<sample_base>( allocator<sample_base>(), 42 );
    observer_ptr<sample_base> first = make_observer( observable );
    EXPECT_EQ( first->get_value(), observable->get_value() );

    observer_ptr<sample_base> second( first );

    EXPECT_EQ( first->get_value(), second->get_value() );
}

TEST( observer_ptr, move_constructor ) {
    observer_ptr<sample_base> first( new sample_base( 42 ) );
    EXPECT_EQ( first->get_value(), 42 );

    observer_ptr<sample_base> second( move( first ) );

    EXPECT_EQ( first.get(), nullptr );
    EXPECT_EQ( second->get_value(), 42 );
}

TEST( observer_ptr, converting_copy_constructor ) {
    observer_ptr<sample_derived> derived( new sample_derived( 42 ) );
    EXPECT_EQ( derived->get_value(), 42 );

    observer_ptr<sample_base> base( derived );

    EXPECT_EQ( base->get_value(), 42 );
    EXPECT_EQ( derived->get_value(), 42 );
}

TEST( observer_ptr, converting_move_constructor ) {
    observer_ptr<sample_derived> derived( new sample_derived( 42 ) );
    EXPECT_EQ( derived->get_value(), 42 );

    observer_ptr<sample_base> base( move( derived ) );

    EXPECT_EQ( base->get_value(), 42 );
    EXPECT_EQ( derived.get(), nullptr );
}

TEST( observer_ptr, reset ) {
    observer_ptr<sample_base> sample( new sample_base( 42 ) );
    EXPECT_TRUE( sample.get() != nullptr );
    sample.reset( nullptr );
    EXPECT_EQ( sample.get(), nullptr );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}