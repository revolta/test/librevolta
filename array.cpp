#include <gtest/gtest.h>

#include "array.hpp"

using namespace ReVolta;

TEST( array, basic_operations ) {
	array<size_t, 6> sample { 0, 1, 2, 3, 4, 5 };
	/* Iterate over all the elements */
	for( size_t check { 0 }; size_t value : sample ) {
		EXPECT_EQ( value, check );
		check++;
	}
	/* Check selected value */
	EXPECT_EQ( sample[ 2 ], 2 );
	/* Check selected value */
	EXPECT_EQ( sample.at( 2 ), 2 );
	/* Front and back elements */
	EXPECT_EQ( sample.front(), 0 );
	EXPECT_EQ( sample.back(), 5 );
	/* Check the size */
	EXPECT_EQ( sample.size(), 6 );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}