#include <gtest/gtest.h>

#include "delegate.hpp"

namespace ReVolta {

    int freestanding_function( int value ) {
        return value * 10;
    }

    class delegate_sample {
    public:
        static int get_value( void ) noexcept {
            return 42;
        }

        delegate_sample( int factor ) noexcept : m_factor( factor ) {}

        int member( int value ) noexcept {
            return value * m_factor;
        }

        int const_member( int value ) const noexcept {
            return value * 2;
        }

    private:
        int m_factor { 1 };
    };

}

using namespace ReVolta;

TEST( delegate, freestanding_function ) {
    delegate<int( int )> freestanding_delegate = delegate<int( int )>::create<&freestanding_function>();
    EXPECT_EQ( freestanding_delegate( 42 ), 420 );
}

TEST( delegate, member_function ) {
    delegate_sample sample ( 2 );
    delegate<int( int )> member = delegate<int( int )>::create<delegate_sample, &delegate_sample::member>( &sample );
    EXPECT_EQ( member( 11 ), 22 );
}

TEST( delegate, const_member_function ) {
    delegate_sample sample ( 10 );
    delegate<int( int )> member = delegate<int( int )>::create<delegate_sample, &delegate_sample::const_member>( &sample );
    EXPECT_EQ( member( 1 ), 2 );
}

TEST( delegate, static_member ) {
    delegate_sample sample ( 3 );
    delegate<int( void )> static_member = delegate<int( void )>::create<&delegate_sample::get_value>();
    EXPECT_EQ( static_member(), 42 );
}

TEST( delegate, lambda ) {
    int touchPoint = 1;
    auto lambda = [&touchPoint](unsigned long a, char b) -> double {
        return a * 0.5;
 	};
    delegate<double( unsigned long, char )> lambda_delegate = delegate<double( unsigned long, char )>::create( lambda );
    EXPECT_EQ( lambda_delegate( 120, 0 ), 60 );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}