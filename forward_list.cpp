#include <gtest/gtest.h>

#include <iostream>

#include "forward_list.hpp"

namespace ReVolta {

	struct element {
		element( void ) noexcept : m_value( 0 ) {}

		element( size_t value ) noexcept : m_value( value ) {}

		size_t get_value( void ) const noexcept {
			return m_value;
		}

	private:
		size_t m_value;
	};

}

using namespace ReVolta;

TEST( forward_list, default_constructor ) {

	forward_list<element> list;

	EXPECT_EQ( list.empty(), true );

	list.emplace_front( 1 );

	list.emplace_front( 2 );

	list.emplace_front( 3 );

	list.insert_after( list.before_begin(), element{ 4 } );

	for( size_t expected_value { 4 }; element & value : list ) {
		EXPECT_EQ( value.get_value(), expected_value-- );
	}

	list.erase_after( list.before_begin() );

	for( size_t expected_value { 3 }; element & value : list ) {
		EXPECT_EQ( value.get_value(), expected_value-- );
	}
}

TEST( forward_list, initializer_list_constructor ) {

	forward_list<size_t> list { 0, 1, 2, 3, 4, 5 };

	for( size_t expected_value { 0 }; const element & value : list ) {
		EXPECT_EQ( value.get_value(), expected_value++ );
	}
}

TEST( forward_list, clear ) {
	forward_list<size_t> list { 0, 1, 2, 3, 4, 5 };
	EXPECT_EQ( list.size(), 6 );
	list.clear();
	EXPECT_EQ( list.size(), 0 );
	EXPECT_EQ( list.empty(), true );
}

TEST( forward_list, iterator_dereference ) {
	forward_list<element> list { 0, 1, 2, 3, 4, 5 };
	typename forward_list<element>::iterator it = list.begin();
	it++;
	EXPECT_EQ( it->get_value(), 1 );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}