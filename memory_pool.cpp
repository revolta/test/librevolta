#include <gtest/gtest.h>

#include "memory_pool.hpp"
#include "type_traits.hpp"

#include <iostream>

namespace ReVolta {
 
    struct alignas( 1024 ) sample_base {
        sample_base( void ) = default;

        sample_base( unsigned char value ) noexcept : m_value{ value } {}

        void base_function( void ) noexcept {}

        unsigned char m_value { 0 };
    };

    template<>
    struct memory_pool_traits<sample_base> {
        using element_type = sample_base;
        static constexpr size_t capacity { 10 };
    };

    memory_pool<sample_base> sample_pool;

    template<>
    memory_pool<sample_base> & pool_allocator<sample_base>::get_pool( void ) noexcept {
        return sample_pool;
    }

}

using namespace ReVolta;

TEST( memory_pool, pool_allocator ) {
    /* Allocate the space for the instace of 'sample_base' from the associated memory pool */
    sample_base * ptr = allocator_traits<pool_allocator<sample_base>>::allocate( pool_allocator<sample_base> {}, 1 );
    /* Construct the instance */
    allocator_traits<pool_allocator<sample_base>>::construct( pool_allocator<sample_base> {}, ptr, 42 );

    EXPECT_EQ( ptr->m_value, 42 );
    EXPECT_EQ( sample_pool.size(), 1 );
    EXPECT_EQ( sample_pool.capacity, 10 );

    sample_base * other_ptr = allocator_traits<pool_allocator<sample_base>>::allocate( pool_allocator<sample_base> {}, 1 );
    EXPECT_EQ( sample_pool.size(), 2 );

    allocator_traits<pool_allocator<sample_base>>::destroy( pool_allocator<sample_base> {}, ptr );
    allocator_traits<pool_allocator<sample_base>>::deallocate( pool_allocator<sample_base> {}, ptr, 1 );
}

int main( int argc, char ** argv ) {
	testing::InitGoogleTest( &argc, argv );

	return RUN_ALL_TESTS();
}